package com.hdbank.cards.offers.service;

import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;

public interface CreditLimitEnhanceService {

	public CreditLimitEnhanceServResp verifyPromocode(String promocode);

}
