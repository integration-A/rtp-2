package com.hdbank.offers.dao;

import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;

public interface CreditLimitDao {

	public CreditLimitEnhanceDaoResp verifyPromocode(String promocode);

}
