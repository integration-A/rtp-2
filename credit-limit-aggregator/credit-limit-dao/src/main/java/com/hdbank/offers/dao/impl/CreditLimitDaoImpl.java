package com.hdbank.offers.dao.impl;

import com.hdbank.offers.dao.CreditLimitDao;
import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;

public class CreditLimitDaoImpl implements CreditLimitDao {

	public CreditLimitEnhanceDaoResp verifyPromocode(String promocode) {
		// this class call to the database and gets the response 
		
		CreditLimitEnhanceDaoResp creditLimitEnhanceDaoResp = new CreditLimitEnhanceDaoResp();
		creditLimitEnhanceDaoResp.setCurrentLimit(50000d);
		creditLimitEnhanceDaoResp.setEligbleAmount(50000d);
		creditLimitEnhanceDaoResp.setExpiryDate("31/09/2019");
		creditLimitEnhanceDaoResp.setPromocode(promocode);
		
		return creditLimitEnhanceDaoResp;
	}

}
