package com.hdbank.cards.offers.resouce;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.hdbank.cards.offers.resouce.response.CreditLimitEnhanceResponse;
import com.hdbank.cards.offers.service.CreditLimitEnhanceService;
import com.hdbank.cards.offers.service.CreditLimitEnhanceServiceImpl;
import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;

/**
 * @author AnilAleti
 *
 *         Sep 5, 2019
 */

@Path("/cle-limit")
public class CreditLimitEnhanceResource {

	private static String healthCheck = "Credit Limit Enhance Service is running fine ..";

	CreditLimitEnhanceService CreditLimitEnhanceService = new CreditLimitEnhanceServiceImpl();

	@GET
	@Path("/health")
	@Produces("text/html")
	public String healthCheck() {

		return healthCheck;

	}

	@Path("/promocode")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response verifyPromocode(@QueryParam("promocode") String promocode) {
		System.out.println("entered into resouce layer verifyPromocode() :" + promocode);

		CreditLimitEnhanceServResp creditLimitEnhanceServResp = CreditLimitEnhanceService.verifyPromocode(promocode);

		System.out.println("response from service layer :" + creditLimitEnhanceServResp);
		CreditLimitEnhanceResponse creditLimitEnhanceResponse = new CreditLimitEnhanceResponse();

		creditLimitEnhanceResponse.setCurrentLimit(creditLimitEnhanceServResp.getCurrentLimit());
		creditLimitEnhanceResponse.setEligbleAmount(creditLimitEnhanceServResp.getEligbleAmount());
		creditLimitEnhanceResponse.setExpiryDate(creditLimitEnhanceServResp.getExpiryDate());
		creditLimitEnhanceResponse.setPromocode(creditLimitEnhanceServResp.getPromocode());

		// it will call service layer and then service will call to dao layer
		/*
		 * 
		 * if (promocode.equals("hdbank100")) { CreditLimitEnhanceResponse
		 * creditLimitEnhanceResponse = new CreditLimitEnhanceResponse();
		 * 
		 * creditLimitEnhanceResponse.setCurrentLimit(70000d);
		 * creditLimitEnhanceResponse.setEligbleAmount(30000d);
		 * creditLimitEnhanceResponse.setExpiryDate("31/09/2019");
		 * creditLimitEnhanceResponse.setPromocode(promocode);
		 * System.out.println("exiting  from resouce layer verifyPromocode() response :"
		 * + creditLimitEnhanceResponse);
		 * 
		 * return Response.ok().status(200).entity(creditLimitEnhanceResponse).build();
		 * 
		 * }
		 * 
		 */
		return Response.ok().status(200).entity(creditLimitEnhanceResponse).build();

	}

	public static void main(String[] args) {

		CreditLimitEnhanceResource resouce = new CreditLimitEnhanceResource();
		System.out.println(resouce.healthCheck());

	}

}
